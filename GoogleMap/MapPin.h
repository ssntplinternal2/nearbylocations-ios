//
//  MapPin.h
//  MapsNearByLocations
//
//  Created by Sword Software on 24/07/19.
//  Copyright © 2019 Sword Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MapPin : NSObject<MKAnnotation> {
    NSString *title;
    NSString *subtitle;
    CLLocationCoordinate2D coordinate;
    
}

@property(nonatomic, copy)NSString *title;
@property(nonatomic, copy)NSString *subtitle;
@property(nonatomic, assign)CLLocationCoordinate2D coordinate;

@end

NS_ASSUME_NONNULL_END
