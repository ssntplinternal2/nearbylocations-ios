//
//  ViewController.m
//  GoogleMap
//
//  Created by Sword Software on 29/07/19.
//  Copyright © 2019 Sword Software. All rights reserved.
//

#import "ViewController.h"
#import "MapPin.h"

@interface ViewController ()<UITableViewDataSource,UITableViewDelegate>{
    CLLocationManager *mgr;
    
    NSMutableArray *listObb;
    NSMutableArray *listObbDetails;
    NSString *img;
    
}
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation ViewController{
   
}


- (void)viewDidLoad {
    [super viewDidLoad];
   
    mgr = [[CLLocationManager alloc]init];
    [mgr requestWhenInUseAuthorization];
    [_mapView setMapType:MKMapTypeStandard];
    
    
    
}


-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    MKCoordinateRegion mapRegion;
    CLLocationCoordinate2D coordinate;
    
    coordinate.latitude = 28.696228;
    coordinate.longitude = 77.173526;
    
    mapRegion.center = coordinate;

    _mapView.centerCoordinate = mapRegion.center;
    
    [_mapView setRegion:MKCoordinateRegionMakeWithDistance(mapRegion.center, 20000, 20000)animated:YES];
    MKPointAnnotation *point = [[MKPointAnnotation alloc]init];
    
    point.coordinate = mapRegion.center;
    point.title = @"this";
    
    
}


- (IBAction)search:(id)sender {
    
    NSMutableArray *infoForList = [[NSMutableArray alloc]init];
    NSMutableArray *infoForListDetails = [[NSMutableArray alloc]init];
    NSString *imgIn;
    MKCoordinateRegion region = {{0.0,0.0},{0.0,0.0}};
    [self arraySetUp];
    [self.table registerClass:[UITableViewCell self] forCellReuseIdentifier:@"Cell"];
    
    NSString *urlString;
    
    
    if ([_userLat.text length]>1&[_userLon.text length]>1) {
            double userCoorLat = [_userLat.text doubleValue];
            double userCoorLon = [_userLon.text doubleValue];
            urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%f,%f&radius=2000&type=restaurant&key=AIzaSyBgdcp65ukTec9bIT_lLrssU2EdujnKFgY",userCoorLat,userCoorLon];
        }
        else{
            urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=28.696228,77.173526&radius=2000&type=restaurant&key=AIzaSyBgdcp65ukTec9bIT_lLrssU2EdujnKFgY"];
        }
    
    NSError *error;
    NSURLResponse *responses;
    
    NSData * data =[NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]] returningResponse:&responses error:&error];
    if (data) {
        NSData *jsonData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        NSArray *results = [jsonData valueForKey:@"results"];
        
        for (int i = 0; i< results.count; i++) {
            NSData *tmpData = [results objectAtIndex:i];
            NSString *geolocation = [tmpData valueForKey:@"geometry"];
            
            
            NSString *icon = [tmpData valueForKey:@"icon"];
            imgIn = icon;
            [infoForList addObject:[tmpData valueForKey:@"name"]];
           
            NSString *nameForMap = [tmpData valueForKey:@"name"];
            NSString *openFlag = [tmpData valueForKey:@"opening_hours"];
            NSString *openIs = [openFlag valueForKey:@"open_now"];
            
            
            if (openIs){
                [infoForListDetails addObject:@"open now"];
            }
            else{
                [infoForListDetails addObject:@"No Information"];
            }
            
            NSString *location = [geolocation valueForKey:@"location"];
            
            double lat = [[location valueForKey:@"lat"] doubleValue];
            double lon = [[location valueForKey:@"lng"] doubleValue];
            
            region.center.latitude = lat;
            region.center.longitude = lon;
            
            MKCoordinateRegion mapRegion;
            CLLocationCoordinate2D coordinate;
            coordinate.latitude = lat;
            coordinate.longitude = lon;
            mapRegion.center = coordinate;
            _mapView.centerCoordinate = mapRegion.center;
            [_mapView setRegion:MKCoordinateRegionMakeWithDistance(mapRegion.center, 2500, 2500)animated:YES];
            
            MKPointAnnotation *point = [[MKPointAnnotation alloc]init];
            point.coordinate = region.center;
            point.title = nameForMap;
            [_mapView addAnnotation:point];
        }
    }
    listObb = [infoForList copy];
    listObbDetails = [infoForListDetails copy];
    img = imgIn;
    
   
    
    [self.table reloadData];
    
}

- (IBAction)setMap:(id)sender {
    switch (((UISegmentedControl *)sender).selectedSegmentIndex) {
        case 0:
            _mapView.mapType = MKMapTypeStandard;
            break;
        case 1:
            _mapView.mapType = MKMapTypeSatellite;
            break;
        case 2:
            _mapView.mapType = MKMapTypeHybrid;
            break;
        default:
            break;
    }
}

- (IBAction)setCoor:(id)sender {
    
    [_userLat setText:@"28.696228"];
    [_userLon setText:@"77.173526"];
    
}


-(void)arraySetUp {
    
}
#pragma mark - UITableView DataSource Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [listObb count];
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    cell.textLabel.text = [listObb objectAtIndex:indexPath.row];
    
    
    //cell.detailTextLabel.text =
    
    NSString *lableOpen = [listObbDetails objectAtIndex:indexPath.row];
    
    if ([lableOpen isEqual:@"open now"]) {
        cell.textLabel.textColor = UIColor.greenColor;
    }
    else if ([lableOpen isEqual:@"No Information"]){
        cell.textLabel.textColor = UIColor.redColor;
    }
    
    cell.imageView.image = [UIImage imageWithData: [NSData dataWithContentsOfURL:[NSURL URLWithString:img]]];
    return cell;
    
}


@end
