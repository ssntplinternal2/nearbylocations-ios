//
//  ViewController.h
//  GoogleMap
//
//  Created by Sword Software on 29/07/19.
//  Copyright © 2019 Sword Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface ViewController : UIViewController <MKMapViewDelegate,UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITextField *userLat;
@property (strong, nonatomic) IBOutlet UITextField *userLon;
@property (strong, nonatomic) IBOutlet UITableView *table;



- (IBAction)search:(id)sender;
- (IBAction)setMap:(id)sender;
- (IBAction)setCoor:(id)sender;

@end

